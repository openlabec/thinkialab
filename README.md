![https://www.docker.com/](https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white)
![https://www.drupal.org/](https://img.shields.io/badge/Drupal-0678BE?style=for-the-badge&logo=drupal&logoColor=white)
![](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![https://www.php.net/](https://img.shields.io/badge/PHP-777BB4?style=for-the-badge&logo=php&logoColor=white)
![](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white)
![https://www.mysql.com/](https://img.shields.io/badge/MySQL-005C84?style=for-the-badge&logo=mysql&logoColor=white)
![https://httpd.apache.org/](https://img.shields.io/badge/Apache-D22128?style=for-the-badge&logo=Apache&logoColor=white)
![https://getbootstrap.com/](https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white)
![https://jquery.com/](	https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white)
![https://www.openstreetmap.org/](https://img.shields.io/badge/OpenStreetMap-7EBC6F?style=for-the-badge&logo=OpenStreetMap&logoColor=white)
![https://analytics.google.com/](https://img.shields.io/badge/Google%20Analytics-E37400?style=for-the-badge&logo=google%20analytics&logoColor=white
) 
![https://git-scm.com/](https://img.shields.io/badge/GIT-E44C30?style=for-the-badge&logo=git&logoColor=white)


<img width="350px" src="web/sites/default/files/Logo%20Thinkia.png">

## Índice
<ul>
	<li><a href="https://gitlab.com/openlabec/thinkialab/-/blob/main/README.md#user-content-descripci%C3%B3n-del-proyecto">Descripci&oacute;n del proyecto</a></li>
	<li><a href="https://gitlab.com/openlabec/thinkialab/-/blob/main/README.md#user-content-acerca-de-drupal">Acerca de Drupal</a>
	<ul>
		<li>Arquitectura l&oacute;gica de Drupal</li>
	</ul>
	</li>
	<li><a href="https://gitlab.com/openlabec/thinkialab/-/blob/main/README.md#user-content-preparaci%C3%B3n-para-la-instalaci%C3%B3n">Preparaci&oacute;n para la instalaci&oacute;n</a>
	<ul>
		<li>Requisitos minimos del sistema</li>
		<li>Instalaci&oacute;n de Docker Compose</li>
		<li>Descarga del proyecto</li>
		<li>Inicio del contenedor con Docker Compose</li>
		<li>Descargar la base de datos</li>
	</ul>
	</li>
	<li><a href="https://gitlab.com/openlabec/thinkialab/-/blob/main/README.md#user-content-instalaci%C3%B3n-del-cms">Instalaci&oacute;n del CMS</a>
	<ul>
		<li>Instalaci&oacute;n b&aacute;sica de Drupal</li>
		<li>Carga de la base de datos</li>
		<li>Configuraci&oacute;n del dominio</li>
	</ul>
	</li>
	<li><a href="https://gitlab.com/openlabec/thinkialab/-/blob/main/README.md#user-content-administraci%C3%B3n-del-cms">Administraci&oacute;n del CMS</a>
	<ul>
		<li>Casos de uso</li>
		<li>Roles</li>
		<li>Manuales de usuario</li>
		<li>Administrador</li>
		<li>Descargar Manual Administrador</li>
	</ul>
	</li>
	<li><a href="https://gitlab.com/openlabec/thinkialab/-/blob/main/README.md#user-content-sobre-la-estructura-del-proyecto">Sobre la estructura del proyecto</a>
	<ul>
		<li>Consideraciones sobre los m&oacute;dulos</li>
		<li>Consideraciones sobre el tema personalizado</li>
		<li>Listado de componentes instalados
		<ul>
			<li>Temas *Contrib* Instalados</li>
			<li>Temas *Custom* Desarrollados</li>
			<li>Modulos *Contrib* Instalados</li>
		</ul>
		</li>
	</ul>
	</li>
</ul>

<p>&nbsp;</p>



# Descripción del proyecto
Thinkiaweb está desarrollado a partir de un requerimiento de funcionalidades para ser un gestor de contenidos manejados por roles de usuarios.

Está construido dentro de un contenedor Docker, en base a los siguientes componentes:

|Componente| Versión | Descripción |
|---|---|---|
| Drupal| 9.5.3 | Sistema de manejo de contenido|
| Drush | 10 | Sistema de gestión del CMS por terminal |
| phpMyAdmin | 5.2.0 | Sistema de gestión de la base de datos |
| Composer | 2.5.5 | Orquestador de librerías PHP |
| Apache2 | 2.4.54 | Servidor Web |
| PHP | 8.1.15 | Lenguaje de programación |
| MySQL | 8.0.32 | Base de datos |
| Highcharts | 11.0 | Librería JavaScrip para la generación de las analíticas |

# Compatibilidad con requerimientos del TDR
|Ambiente|Nombre componente|Versión|Detalle|
|---|---|---|---|
|Sistema operativo|Centos|7.9|Sistema operativo del servidor sobre el cual se ejecutarán los servicios de aplicaciones y base de datos. <br> <br>Debido a que el sistema se encuentra dockerizado, el sistema operativo base es independiente, por lo que Centos es recomendable para la instalación del sistema.|
|Servidor Web|Apache|2.4.54|Servidor web encargado de ejecutar los módulos del sistema.<br><br>El servidor web dentro del contenedor Docker funciona con la misma versión mencionada. Sin embargo, debido a la dockerización, para la salida a web el sistema puede usar otro servidor o continuar utilizado Apache.|
|Gestor de la base de datos|PostgresSQL|14.5|El sistema actual funciona con MYSQL 8.0.32, y no con Postgres, debido a la mejor disponibilidad que tiene con Drupal, que es el sistema base sobre el que se construyó la aplicación. <br><br>No existiría ningún problema con el servidor debido a que el sistema está construido dentro de un contenedor de docker que hace independiente el funcionamiento óptimo de sus componentes por fuera del sistema del servidor. <br><br>escogimos mysql por los siguientes factores:<br><br> 1.Drupal se desarrolla principalmente en MySQL, por lo que algunas características específicas de Drupal funcionan mejor o estan más probadas en MySQL. <br><br>2 Rendimiento,   MySQL tiene una ventaja en rendimiento en entornos de cargas de lectura intensivas contra postgresSQL y es mas escalable para sistemas que sistemas que no tienenen escrituras o consultas complejas, lo que es el caso en thinkialab |
|Lenguajes de programación|PHP|8.1|Lenguaje de programación de las diferentes funcionalidades o módulos del sistema.<br><br> El sistema utiliza la versión de PHP 8.1.15. Dentro del contenedor docker.|
|Framework MVC|Laravel|9.0|El sistema no fue construido sobre Laravel, sino sobre el CMS Drupal que a su vez es construido sobre Symphony. <br><br>La decisión de utilizar un CMS fue debido a los componentes ya construidos y probados, al mismo tiempo que permite mayor flexibilidad para la programación del sistema.|
|Framework front-end|Bootstrap|5.2.1|Framework utilizado para el desarrollo de las interfaces de usuario del sistema.|
|Librería|Node.js|16.17.0|Entorno de ejecución de Javascript para ejecutar librerías del sistema. <br><br>No se ha utilizado Nodejs para el desarrollo del sistema debido a que el diseño basado en Drupal no lo ha requerido.|

# Acerca de Drupal
Drupal es un sistema de gestión de contenido (CMS, por sus siglas en inglés) de código abierto y gratuito. Fue creado en 2001 por Dries Buytaert y se ha convertido en uno de los CMS más populares del mundo, utilizado por empresas, organizaciones gubernamentales, instituciones académicas y personas individuales para crear sitios web de todo tipo y tamaño.

Drupal es altamente personalizable y flexible, lo que lo hace ideal para proyectos web complejos y grandes. Tiene una gran cantidad de módulos y temas disponibles para agregar funcionalidades y cambiar la apariencia de un sitio web, y su arquitectura modular permite a los desarrolladores crear y compartir fácilmente sus propias extensiones.

Además, Drupal es conocido por su seguridad, escalabilidad y capacidad de manejar grandes volúmenes de contenido y tráfico web.

## Arquitectura lógica de Drupal
![](img-readme/arquitecturadrupal.png) 

# Preparación para la instalación
Todos los siguientes pasos deben ser realizados en una terminal Linux, dentro del servidor donde será alojado el proyecto.

El tiempo de instalación está calculado entre 20 a 30 minutos, en función de que el sistema base (servidor) esté correctamente configurado y de la disponibilidad de la conexión a internet, que es necesaria para descargar el proyecto y sus contenedores.

**Nivel de dificultad:** Medio.

Nota: Todo el proceso debe ser realizado por un administrador de sistemas, con un par de años de experiencia en instalación de sistemas de manejo de contenido (CMS), con conocimientos básicos sobre contenedores en Docker y sistema operativo Linux para la administración de servidores.

## Requisitos minimos del sistema
- Servidor Linux (Debian, Centos, etc)
- RAM 256MB
- git
- curl
- docker compose

## Instalación Docker
Instalamos Docker en el sistema añadiendo el repositorio, actualizando e instalando.
```sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
```
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```
```
sudo apt update
```
```
sudo apt install -y docker-ce docker-ce-cli containerd.io
```
Habilitamos Docker en el sistema
```
sudo systemctl unmask docker
```
```
sudo systemctl start docker
```

Comprobamos la versión de Docker
```
docker --version
```

Realizamos una prueba de ejecución de Docker
```
docker run hello-world
```

## Instalación de Docker Compose

El primer paso es descargar la versión 2.13.0 de Docker Compose, esto se lo puede hacer con el siguiente comando.
```sh
sudo curl -L "(https://github.com/docker/compose/releases/download/v2.13.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

```

Luego lo convertimos en ejecutable
```sh
sudo chmod +x /usr/local/bin/docker-compose
```

## Descarga del proyecto

Se puede seguir el proceso de instalación con ayuda de un video, que se accede pulsando la miniatura

<a target="_blank" href="https://youtu.be/ZDj6GQ9dbG8"><img width="300px" src=img-readme/videoinstalacion.png></a>

Definimos en un carpeta del servidor, donde queremos alojar el proyecto.

Descargamos el proyecto en nuestro servidor: 

```sh
git clone https://gitlab.com/openlabec/thinkialab.git
```

Ingresamos a la carpeta que se ha creado:
```sh
cd thinkialab
```


## Inicio del contenedor con Docker Compose

Para iniciar el contenedor de este proyecto, bastará ejecutar el comando:

   ```sh
   docker-compose up -d
   ```

Esto inicia el contenedor del proyecto descargando todos los componentes necesarios.

Cuando finalice todo el proceso, en la terminal ejecutaremos el comando

```sh
docker ps
```
Deberá generar una salida paracida a esta:

```
CONTAINER ID   IMAGE                   COMMAND                  CREATED       STATUS       PORTS                                   NAMES
3610127e7821   thinkialab-drupal       "docker-php-entrypoi…"   6 weeks ago   Up 6 weeks   0.0.0.0:9998->80/tcp, :::9998->80/tcp   thinkia
59fc1a8e41de   mysql:8                 "docker-entrypoint.s…"   6 weeks ago   Up 6 weeks   3306/tcp, 33060/tcp                     thinkia_db
e203f7bf0ba0   phpmyadmin/phpmyadmin   "/docker-entrypoint.…"   6 weeks ago   Up 6 weeks   0.0.0.0:9999->80/tcp, :::9999->80/tcp   thinkia_db_admin
```

Si el servidor web está funcionando con Apache,
para comprobar que los sistemas están en funcionamiento, se puede en un navegados ingresar a: 

- [localhost:9998](http://localhost:9998/) Donde estará alojada la página web.

- [localhost:9999](http://localhost:9999/) Donde se encuentra el cliente MySQL, phpMyAdmin desde donde controlaremos la base de datos en un primer momento.

Si el servidor está funcionando con NGIX, hay que configurar una redirección a los puertos internos y reemplazar "localhost" con la ip del servidor para que sea visible desde Internet.

## Descargar la base de datos
El siguiente paso consiste en descargar la base de datos del sitio, provista de forma independientemente al repositorio.

La base de datos será cargada por medio de phpMyAdmin por medio de su interfaz gráfica.


# Instalación del CMS

En los pasos anteriores hemos preparado el proyecto en el servidor, poniendo en funcionamiento Docker Compose, descargando y ejecutando los contenedores.

En la siguiente parte, vamos a instalar el sistema Drupal en el servidor, que servirá de base, previamente a la acción de subir la base de datos del proyecto.

## Instalación básica de Drupal

- Ingrese a [localhost:9999](http://localhost:9998/).

- Elija en el menú de idiomas "Español".

<img src=img-readme/4thinkia.png>

- Luego elija el perfíl de instalación **estandar**.

<img src=img-readme/7thinkia.png>

- Posteriormente el instalador le solicitará dar permisos de escritura a algunas carpetas y archivos.

<img src=img-readme/5thinkia.png>

```
cd web/sites/default/
```
```
chmod 777 settings.php
```
```
chmod -R 777 files
```
```
chmod -R 777 files/translations
```

En caso de que el sistema le solicitara las credenciales de la base de datos, agregue los datos de configuración que están disponibles en el archivo *.env*
con la siguiente nomenclatura: 
- Database name = DB
- Database user = DBU
- Dababase password = DBP 
- Database host = DBH

Al dar clik en continuar, iniciará el proceso de descarga e instalación de todos los componentes de Drupal. 

<img src=img-readme/8thinkia.png>

Cuando concluya el proceso agregue los datos de información que le solicitan. Estos pueden ser datos de prueba aleatorios, la finalidad es de poner el sistema en funcionamiento temporal (luego esos datos serán borrados).

<img src=img-readme/9thinkia.png>

La instalación concluirá mostrándo una página de bienvenida. 

<img src=img-readme/10thinkia.png>

No se preocupe si no ve la página de Thinkia, ya que aún no hemos cargado la base de datos del proyecto. Eso será el siguiente paso. 


## Carga de la base de datos
Para cargar la base de datos al Drupal instalado y concluir con el proceso de instalación del sistema, realice los siguientes pasos:

- En una nueva ventana ingrese a phpyMyAdmin [localhost:9999](http://localhost:9999/)

- seleccione en la barra de la izquierda "thinkia_db"

<img src=img-readme/1thinkia.png>

- en el parte inferior de la página, seleccione todos los elementos haciendo click en ***check all***

<img src=img-readme/2thinkia.png>

- posteriormente haga clic en ***whith selected*** y marque en el menú desplegable "drop|  | acepte borrar todos los datos de la base.

<img src=img-readme/3thinkia.png>

- Luego seleccione la pestaña "Importar"
- Agregue la base de datos descargada previamente

<img src=img-readme/6thinkia.png>

- Presione "import"
- Al finalizar la carga, regrese a la página del sitio.
- Ya estará disponible la web de Thinkia, visible en: [localhost:9999](http://localhost:9998/)

<img src="img-readme/11thinkia.png">

Después de este paso, se debe desconectar phpMyAdmin de la salida a Internet.

## Configuración del dominio
Para configurar el dominio, es importante previamente decidir cuál *servidor web* se utilizará: **Apache** o **Nginx**.

### Instalación Apache2
```
sudo apt update
```
```
sudo apt install apache2
```
```
sudo service apache2 start
```

### Instalación Nginx
```
sudo apt update
```
```
sudo apt install nginx
```
```
sudo service nginx start
```

Use la configuración de Apache o Nginx para configurar la salida del sitio de los contenedores de Docker hacia Internet.


### Configurar el dominio con Nginx
En la carpeta 
```
cd /etc/nginx/sites-available/
```
crear un archivo con el nombre del dominio, en este caso usaremos *thinkialab.info*, reemplazar por el dominio real.
```
sudo vi thinkialab.info
```
dentro del archivo, configurar de la siquiente forma: 
```
server {
     server_name thinkialab.info;

     location /.well-known/acme-challenge {
        root /var/www/;
    }

    location / {

      proxy_set_header        Host $host;
      proxy_set_header        X-Real-IP $remote_addr;
      proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header        X-Forwarded-Proto $scheme;
      proxy_pass          http://0.0.0.0:9998;
      proxy_read_timeout  90;
    }
}
```
Finalmente, habilitar el dominio en la carpeta */sites-enabled*.
```
sudo ln -s /etc/nginx/sites-available/thinkialab.info /etc/nginx/sites-enabled
```
```
sudo service nginx restart
```

### Configurar el dominio con Apache2
Para configurar el dominio con Apache, se deben seguir los mismos pasos que en la configuración de Nginx pero dentro de la carpeta de configuración de Apache.

En este caso se puede cambiar http://localhost:9998 por el nombre del dominio en el archivo **.conf** de /etc/apache2/sites-available/thinkialab.conf

### Certificado SSL

Posterioremente, agregue un **certificado ssl** al dominio. Esto lo puede hacer usando el servicio de [Certbot](https://certbot.eff.org/), proporcionado por EFF.

# Administración del CMS

## Casos de uso
![](img-readme/casosdeuso.png)

## Roles
El proyecto está construido para contener 4 roles listados en orden de menores a mayores privilegios:

| Rol | Descripción |
|----|----|
| Usuario anónimo | Visitante ocasional al sitio, no necesita ningún tipo de credenciales |
| Colaborador | Este rol se activa con solo crear una cuent en el sitio, puede crear contenido, editar y borrar sus propios contenidos sin afectar al resto de la informción |
| Editor | Este rol solo puede ser adquirido cuando otro editor le daa privilegios a una cuenta de usuario. Este rol puede crear, editar y borrar sus propios contenidos y otros. Es un rol reservado solo para los usuarios de mayor confianza. |
| Administrador | Es el rol con mayores privilegios, su uso debe estar restringido a perfiles técnicos ya que puede poner en riesgo el funcionamiento del sitio. |

## Manuales de usuario
La administración del sistema es bastante intuitiva para los roles **colaborador** y **editor**. Sin embargo el rol **administrador** debe conocer fundamentos básicos de Drupal, mismos que pueden ser consultados en la basta documentación del proyecto provista en su [sitio web](https://www.drupal.org/documentation), y/o con la documentación específica de cada *modulo*.

Adjuntamos una serie de tres videos, a modo de manuales, que sirven para conocer la adminstración del sitio de forma básica.

| Rol de usuario | Video Manual |
|----|----|
| Rol Colaborador | <a target="_blank" href="https://youtu.be/hh-0tklYdbE">Ver video</a> |
| Rol Editor | <a target="_blank" href="https://youtu.be/8C1xzeiwuvg">Ver video</a> |
| Rol Administrador | <a target="_blank" href="https://youtu.be/7Te2ZJlwTgQ">Ver video</a> |

## Administrador

Para ingresar al administrador de Drupal, se debe agregar a la dirección /user a la URL, ejemp:

https://midominio/user
o haciendo clic en el botón iniciar sesión.

Agregue las credenciales del administrador que le son proporcionadas con el proyecto.

Al iniciar sesión como administrador encontrará una interfaz más compleja que los roles de editor y colaborador. 

El modo administrador solo debería ser usado para realizar cambios en la configuración que no son posibles hacerlos por el rol editor, sin embargo su uso debe estar reservado a un perfíl técnico para no comprometer el funcionamiento del sitio.

## Descargar Manual Administrador

Es posible descargar una versión básica del manual para el adminstrador en formato de texto, el cuál puede ser accedido desde [descarga](img-readme/manual.pdf).

# Sobre la estructura del proyecto

ThinkiaWeb está desarrollado básicamente sobre:
- Drupal 9
- MySql
- Bootstrap 5

Drupal, a parte del sistema *core* (que por ninguna razón debe ser modificado), compone de dos secciones modulares: los temas y los módulos, los cuales pueden a su vez ser de tipo *Contrib*, desarrollados por la comunidad y sujetos a pruebas de validación, o *Custom*, que son desarrollos personalizados para el proyecto.

## Consideraciones sobre los módulos
ThinkiaWeb, para optimizar las funcionalidades construye el sitio en base a un tejido articulado de modulos *contrib* en base a los requerimiento de funcionalidades.

En relación con la funcionalidad del sitio, como toda la construcción está basada en módulos *contrib*, probados y testeados por la comunidad de Drupal, no se espera que puedan generar algún fallo.

## Consideraciones sobre el tema personalizado
Sin embargo, la apariencia gráfica es un desarrollo *custom*, basada en Bootstrap 5 como framework frontend, altamente mantenible en el tiempo.

Para el mantenimiento del tema, se debe tomar en consideración que este tiene componentes construidos en base a:

- Twig
- Javascript
- HTML 5
- CSS 3

Fundamentalmente, muchas de las vistas de la página son construidas en base a plantillas de Twig.

## Listado de componentes instalados

### Temas *Contrib* Instalados
| Componente | Versión | Descripción |
|---|---|---|
| [Barrio](https://www.drupal.org/project/bootstrap_barrio) | 5.5.9 | Tema base, es una versión adaptada de Bootstrap 5 que sirve como lienzo para el proyecto |
### Temas *Custom* Desarrollados

| Componente | Versión | Descripción |
|---|---|---|
| ThinkiaTheme | 5.5.6 | Adjuto en el proyecto. Es un subtema de Barrio, modificado y altamente personalizado para ThinkiaWeb. Es dependiente operativamente de Barrio. |

### Modulos *Contrib* Instalados
| Componente | Versión | Descripción |
|---|---|---|
| [Address](https://www.drupal.org/project/address) | 1.11 | Proporciona funcionalidades para gestionar direcciones postales. |
| [Geocoder](https://www.drupal.org/project/geocoder) | 3.31 | Un API para geocodifcar y revertir geodata en otros tipos de datos GIS |
| [Geomap Field](https://www.drupal.org/project/geomap_field) | 1.0.0  | Proporciona un campo para gestionar direcciones con el fin de geolocalizar y visualizar en mapas | 
| [Svg Image](https://www.drupal.org/project/svg_image) | 1.16 | Da soporte a archivos svg |
| [Admin toolbar](https://www.drupal.org/project/admin_toolbar) | 3.3.0 | Menu de administración |
| [Geofield](https://www.drupal.org/project/geofield) | 1.52 | Almacena datos geográficos y de localización |
| [Leaflet](https://www.drupal.org/project/leaflet) | 2.2.12 | Integra con el sistema de mapas de Leaflet |
| [Conditional Fields](https://www.drupal.org/project/conditional_fields) | 4.0.0-alpha2 | Define dependencias entre campos y valores |
| [Geofield Map](https://www.drupal.org/project/geofield_map) | 3.0.6 | *Widget* de mapa, formateador e integrados a *views* |
| [Libraries](https://www.drupal.org/project/libraries) | 4.0.0 | Permite el uso compartido y dependientes de la versión de bibliotecas externas |
| [Views bootstrap](https://www.drupal.org/project/views_bootstrap) | 5.5.0-alpha1 | Permite diferentes estilos para ser usado en *views* con componentes de Bootstrap5 |
| [Ctools](https://www.drupal.org/project/ctools) | 3.13 | Proporciona una serie de utilidades y APIs de ayuda para desarrolladores de Drupal y constructores de sitios |
| [Geolocation](https://www.drupal.org/project/geolocation) | 3.12 | Campo de localización para guardar y mostrar geodata |
| [Map Provider](https://www.drupal.org/project/map_provider) | 1.0.0 | Permite usar y definir multiples proveedores de mapas usando un API |
| [Font Awesome](https://www.drupal.org/project/fontawesome) | 2.24 | Caja de herramientas y set de íconos |
| [Geolocation Provider](https://www.drupal.org/project/geolocation_provider) | 1.0.1 | Permite a desarrolladores multiples proveedores de geolocalización |
| [Menu Link Attributes](https://www.drupal.org/project/menu_link_attributes) |  1.3 | Permite agregar atributos a enlaces de menú |
| [Better Expose filters](https://drupal.org/project/better_exposed_filters)| 6.0| Permite mejorar la exposición de filtros |
| [Better Search](https://drupal.org/project/better_search)| 1.6| Mejora la experiencia de búsqueda |
| [Charts](https://drupal.org/project/charts)| 5.0| API para integrar gráficos de datos junto a **views** y la librería Highcharts |
| [Color Field](https://drupal.org/project/color_field)| 3.0| Campo de selección de color |
| [Country](https://drupal.org/project/country)| 1.1| Campo de selección de país |
| [Devel](https://drupal.org/project/devel)| 5.0| Herramientas e interfaz para desarrolladores |
| [Empty Front Page](https://drupal.org/project/empty_front_page)| 1.1| Vista de pantalla inicial límpia |
| [Entity Connect](https://drupal.org/project/entityconnect)| 2.0@RC| Permite referenciar y crear entidades |
| [Field Token Value](https://drupal.org/project/field_token_value)| 3.0| Campo que permite referenciar tokens |
| [Hide Revision Field](https://drupal.org/project/hide_revision_field)| 2.2| Controla la visibilidad de los campos de revisión |
| [Libraries](https://drupal.org/project/libraries)| 4.0| Permite el uso de librerías externas |
| [No Current Pass](https://drupal.org/project/nocurrent_pass)| 1.2| Oculta el requerimiento de contraseña actual |
| [Node Access](https://drupal.org/project/nodeaccess)| 1.1| Provene accesos de control por nodos |
| [Paragraphs](https://drupal.org/project/paragraphs)| 1.15| Permite la creación de entidades de Paragraphs |
| [Pathauto](https://drupal.org/project/pathauto)| 1.11| Permite personalizar la URL del nodo |
| [Redirect 403 to User Login](https://drupal.org/project/r4032login)| 2.2| Redirige a los usuarios anónimos de las páginas de error 403 (Acceso denegado) |
| [Redirect after Login](https://drupal.org/project/redirect_after_login)| 2.7| Redirige al usuario después del inicio de sesión |
| [Registration Role](https://drupal.org/project/registration_role)| 1.1| Asigna un rol a los nuevos usuarios registrados |
| [Responsive Menu](https://drupal.org/project/responsive_menu)| 4.4| Provee un menú responsivo basado en Javascript |
| [Select2](https://drupal.org/project/select2)| 1.14| Integra la librería Select2 |
| [Simple Login](https://drupal.org/project/simplelogin)| 6.0| Prové un template configurable para el inicio de sesión |
| [SMTP](https://drupal.org/project/smtp)| 1.2| Permite la conexión SMTP con un servicio de correo |
| [Subpathauto](https://drupal.org/project/subpathauto)| 1.3| Proporciona soporte para extender sub-rutas de alias de URL |
| [Taxonomy Menu](https://drupal.org/project/taxonomy_menu)| 3.4| Embebe una menú como árbol  |
| [Twig Debugger](https://drupal.org/project/twig_debugger)| 1.1| Habilita la depuración de Twig |
| [Twig Tweak](https://drupal.org/project/twig_tweak)| 3.2| Proporciona algunas funciones y filtros extras de Twig |
| [Views Exposed Form Layout](https://drupal.org/project/vefl)| 3.0| Proporciona funcionalidades de vistas expuestas |
| [Views Bootstrap](https://drupal.org/project/views_bootstrap)| 5.5@alpha| Propociona una inerfaz de consulta para Bootstrap |
| [Views Data Export](https://drupal.org/project/views_data_export)| 1.2| Plugin para exportar data |
| [Views Infinite Scroll](https://drupal.org/project/views_infinite_scroll)| 2.0| Un paginador que funciona con ajax |
| [XLS Serialization](https://drupal.org/project/xls_serialization)| 1.3| Proporciona un servicio para (des)serializar datos a/desde formatos como JSON y XML |
| [Google Analytics](https://www.drupal.org/project/google_analytics)|4.0.2 | Agrega un sistema de rastreo por Google Analytics |
| [Responsive Favicons](https://www.drupal.org/project/responsive_favicons)| 2.0.0 | Favicons con diferentes tamaños para diferentes interfaces |
| geocoder-php/chain-provider| 4.5|  |
| geocoder-php/google-maps-provider| 4.7|  |
| phpoffice/phpspreadsheet| 1.26 |
| [Hihgcharts](https://www.highcharts.com/) | 11.0 |
